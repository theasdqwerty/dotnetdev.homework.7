﻿using System;
using System.Data;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using WebClient.Infrastructure;

namespace WebClient
{
    static class Program
    {
        static Task Main(string[] args)
        {
            var services = new ServiceCollection();
            services.AddHttpClient("homework7", 
                client => client.BaseAddress = new Uri("https://localhost:7001/customers") );
            services.AddTransient<ICustomerGenerator, CustomerGenerator>();
            
            return Task.CompletedTask;
        }

        private static CustomerCreateRequest RandomCustomer()
        {
            throw new NotImplementedException();
        }
    }
}