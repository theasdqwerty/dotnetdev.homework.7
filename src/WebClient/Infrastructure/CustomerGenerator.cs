﻿namespace WebClient.Infrastructure;

public class CustomerGenerator : ICustomerGenerator
{
    public Customer Generate()
    {
        return new Customer() {Firstname = Faker.Name.First(), Lastname = Faker.Name.Last()};
    }
}