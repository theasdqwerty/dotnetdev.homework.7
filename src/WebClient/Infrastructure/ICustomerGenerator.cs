﻿namespace WebClient.Infrastructure;

public interface ICustomerGenerator
{
    public Customer Generate();
}